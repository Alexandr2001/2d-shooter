﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControls : MonoBehaviour {
	public void PlayPressed() {
		SceneManager.LoadScene("level1");
	}

	public void ExitPressed()
	{
		Application.Quit();
	}
}

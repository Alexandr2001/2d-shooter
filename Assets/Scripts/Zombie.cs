﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEngine;

public class Zombie : MonoBehaviour
{

	Animator anim;
	Rigidbody2D rb;
	private GameObject hero;
	private float run;
	private bool whenLook;
	public int health = 2;

	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		hero = GameObject.FindWithTag("Player");
	}

	void Update()
	{
		if (hero.transform.position.x < transform.position.x + 1f)
		{
			run = -1f;
		}
		else if (hero.transform.position.x > transform.position.x - 1f)
		{
			run = 1f;
		}
		else
		{
			run = 0f;
		}
		rb.velocity = new Vector2(run, rb.velocity.y);
	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player") {
			HealthBar.AdjustCurrentValue(-10);
		}
	}
	void FixedUpdate()
	{
		TurnForTheHero();
	}

	private void TurnForTheHero()
	{
		if (hero.transform.position.x < transform.position.x && whenLook == false)
		{
			Flip();
		}
		if (hero.transform.position.x > transform.position.x && whenLook == true)
		{
			Flip();
		}
	}

	private void Flip()
	{
		whenLook = !whenLook;
		transform.Rotate(0f, 180f, 0f);
	}

	public void takeDamage(int damage)
	{
		health -= damage;

		if (health <= 0) Die();
	}

	private void Die()
	{
		anim.SetTrigger("dead");
		var collider = GetComponent<BoxCollider2D>();
		collider.offset = new Vector2(0f, 1f);
	}

	void DestroyMyObject()
	{
		Destroy(gameObject);
		hero.GetComponent<Counter>().countUp();
	}
}
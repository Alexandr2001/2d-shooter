﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour {

	public GameObject recordCounter;
	private static int targetCount = 1;

	public void countUp() {
		recordCounter.GetComponent<Text>().text = targetCount++.ToString();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContButton : MonoBehaviour {

	private PlayerControl player;

	void Start()
	{
		player = FindObjectOfType<PlayerControl>();
	}
	public void LeftArrow()
	{
		player.moveright = false;
		player.moveleft = true;
		player.anim.SetBool("isRunning", true);
	}
	public void RightArrow()
	{
		player.moveright = true;
		player.moveleft = false;
		player.anim.SetBool("isRunning", true);
	}
	public void ReleaseLeftArrow()
	{
		player.moveleft = false;
		player.anim.SetBool("isRunning", false);
	}
	public void ReleaseRightArrow()
	{
		player.moveright = false;
		player.anim.SetBool("isRunning", false);
	}

	public void FireArrow() 
	{
		player.fire = true;
	}

	public void ReleaseFireArrow() 
	{
		player.fire = false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class deadMenu : MonoBehaviour {

    public void exitInMenu() {
        SceneManager.LoadScene("menu");
    }

    public void continueGame() {
        SceneManager.LoadScene("level1");
    }
}

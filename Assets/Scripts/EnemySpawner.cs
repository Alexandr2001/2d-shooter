﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public Transform spawnPointLeft;
	public Transform spawnPointRight;
	public GameObject enemy;
	public float timeSpawn;

	void Start () {
		StartCoroutine(SpawnCD());
	}

	IEnumerator SpawnCD() {
		yield return new WaitForSeconds(timeSpawn);
		Instantiate(enemy, spawnPointRight.position, Quaternion.identity);
		Instantiate(enemy, spawnPointLeft.position, Quaternion.identity);
		Repeat();
	}

	private void Repeat()
	{
		StartCoroutine(SpawnCD());
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour {

	// main field
	public Animator anim;
	public Rigidbody2D RigBud;
	public float movespeed;
	public bool moveright;
	public bool moveleft;
	public bool fire;
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask WoIG;

	// camera
	public Camera cam;
	private float widthCam;
	private Vector2 centrCam;
	private float minX, maxX;

	// fire
	public Rigidbody2D bullet;
	public Transform firePoint;
	public float fireRate = 1f;
	private float curTimeout;

	void Start () {
		RigBud = GetComponent <Rigidbody2D> ();
		anim = GetComponent<Animator>();

		widthCam = cam.orthographicSize * cam.aspect;
		centrCam = cam.transform.position;
		minX = centrCam.x - widthCam;
		maxX = centrCam.x + widthCam;
	}

	void Update () {
		if (HealthBar.currentValue <= 0)
		{
			DeathProtocol();
		}
		if (moveright && transform.position.x < maxX - 1f) {
			RigBud.velocity = new Vector2 (movespeed, RigBud.velocity.y);
			transform.rotation = Quaternion.Euler (0, 0, 0);
		}
		if (moveleft && transform.position.x > minX + 1f) {
			RigBud.velocity = new Vector2 (-movespeed, RigBud.velocity.y);
			transform.rotation = Quaternion.Euler (0, 180, 0);
		}
		if (fire)
		{
			Fire();
		}
		else
		{
			curTimeout = 100;
		}
	}

	public void Fire()
	{
		curTimeout += Time.deltaTime;
		if (curTimeout > fireRate)
		{
			curTimeout = 0;
			Instantiate(bullet, firePoint.position, firePoint.rotation);
		}
	}

	private void DeathProtocol() {
		SceneManager.LoadScene("gameOver");
	}
}

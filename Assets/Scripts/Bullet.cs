﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour {

	public float speed = 27f;
	public Rigidbody2D rb;
	private const int takeDamage = 1;

	void Start () {
		rb.velocity = transform.right * speed;
	}

	private void OnTriggerEnter2D(Collider2D hitInfo) {
		Zombie enemy = hitInfo.GetComponent<Zombie>();
		if (enemy != null) {
			enemy.takeDamage(takeDamage);
		}
		Destroy(gameObject);
	}
}
